call plug#begin()
" Plug 'tabnine/YouCompleteMe'           " TabNine from Outer space
  Plug 'nathanaelkane/vim-indent-guides' " Indent guides
  Plug 'junegunn/vim-easy-align'         " Align things
  Plug 'tpope/vim-unimpaired'            " More movement cmds
" Plug 'bling/vim-bufferline'            " Show buffers up top
  Plug 'tpope/vim-surround'              " Surround things with () etc
  Plug 'tpope/vim-sensible'              " Defaults
  Plug 'tpope/vim-fugitive'              " Git
" Plug 'tommcdo/vim-fubitive'            " bickbucket for fugitive
  Plug 'ctrlpvim/ctrlp.vim'              " Open things
  Plug 'bling/vim-airline'               " Lower ui
  Plug 'mileszs/ack.vim'                 " Faster grep
  Plug 'vim-scripts/Gundo'               " Undo viz
  Plug 'chaoren/vim-wordmotion'          " CamelCase and snake_case recognized as words
  Plug 'thaerkh/vim-workspace'           " Workspaces!
" Plug 'OrangeT/vim-csharp'              " C# syntax
  Plug 'ap/vim-css-color'                " Shew colors in css
" Plug 'rizzatti/dash.vim'               " Documentation
  Plug 'kshenoy/vim-signature'           " Shew marks in sidebar
" Plug 'bkbncn/vim-colorschemes-picker'  " Random colors...
  Plug 'tbastos/vim-lua'                 " Lua formating
" Plug 'StanAngeloff/php.vim'            " PHP formating
" Plug 'rayburgemeestre/phpfolding.vim'  " PHP folding

  " Colorschemes
  Plug 'robertmeta/nofrils'
  Plug 'vim-airline/vim-airline-themes'
  Plug 'rainglow/vim'
  Plug 'tssm/fairyfloss.vim'
call plug#end()

runtime! plugin/sensible.vim

colorscheme nofrils-dark
syntax enable
"filetype plugin on

set backupdir=~/.vimswap//
set undodir=~/.vimswap//
set directory=~/.vimswap//

set breakindent
set colorcolumn:108
set cursorline
set expandtab
set hidden
set hlsearch
set ignorecase
set linebreak
set nocp
set nu
set relativenumber
set scrolloff=0
set shiftwidth=2
set smartcase
set smartindent
set smarttab
set softtabstop=0
set tabstop=2
set wrap

"set foldmarker=//region,//endregion
"set foldmethod=marker
"set belloff=all

let g:python_recommended_style=0

if has("gui_running")
  set guifont=ProggyTinyTT:h16
  set noantialias

  set guioptions-=m  "remove menu bar
  set guioptions-=T  "remove toolbar
  set guioptions-=r  "remove right-hand scroll bar
  set guioptions-=L  "remove left-hand scroll bar
  set guioptions-=e  "remove tab bar

  let g:airline_theme='bubblegum'
" let g:airline_powerline_fonts = 1
  let g:airline#extensions#tabline#enabled = 1
  let g:airline#extensions#tabline#show_buffers = 1
  let g:airline#extensions#tabline#show_tabs = 1
  let g:airline#extensions#tabline#show_splits = 1

" let g:shell_fullscreen_always_on_top = 0
" let g:shell_fullscreen_items = 'm'

  set fillchars+=vert:│
  hi VertSplit guibg=NONE

  set transp=10
  set blurradius=90
endif

"if has("multi_byte")
  "if &termencoding == ""
    "let &termencoding = &encoding
  "endif
  "set encoding=utf-8
  "setglobal fileencoding=utf-8
  ""setglobal bomb
  "set fileencodings=ucs-bom,utf-8,latin1
"endif

let mapleader=","

map <silent> j gj
map <silent> k gk

nnoremap <leader>/ :noh<CR>

nnoremap <tab> <C-W>w
nnoremap <S-tab> <C-W>W

nnoremap <F5> :make<CR>

" Fugitive
nnoremap <leader>gs :Git<CR>
nnoremap <leader>gp :Git push<CR>

" Ctrl-p
"let g:ctrlp_extensions = ['line']
"nnoremap <C-l> :CtrlPLine<CR>
"let g:ctrlp_user_command = 'find %s -type f'
"let g:ctrlp_user_command = 'ag %s -1 --nocolor -g ""'
"let g:ctrlp_user_command = ['.git/', 'git --git-dir=%s/.git ls-files -oc --exclude-standard']
let g:ctrlp_user_command = ['.git', 'cd %s && git ls-files -co --exclude-standard']
nmap <c-l> :CtrlPLine<cr>
lmap <c-k> :CtrlPBuffer<cr>

" Gundo
let g:gundo_prefer_python3 = 1
nnoremap <leader><leader>u :GundoToggle<CR>

" Workspace
let g:workspace_undodir=$HOME.'/.vimswap/'
let g:workspace_session_directory=$HOME.'/.vimswap/'
let g:workspace_autosave = 0
let g:workspace_autosave_untrailspaces = 0
nnoremap <leader><leader>w :ToggleWorkspace<CR>

" Easy align
xmap ga <Plug>(EasyAlign)
nmap ga <Plug>(EasyAlign)

" ag
cnoreabbrev ag Ack
let g:ackprg = 'ag -U --vimgrep'

" Custom, sort by length
command! -range Sort <line1>,<line2>!awk '{ print length, $0 }' | sort -nr | cut -d" " -f2-

" Dash
"nmap <silent> <leader>d <Plug>DashSearch
